package com.oksin.video.pepevideolari;

import android.content.Context;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.GoogleApiAvailability;


public class Utils {

    public static Tracker tracker;
    public static GoogleAnalytics analytics;

    public static Tracker getTracker(Context context, String screenName) {

        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
   /*     int result = googleAPI.isGooglePlayServicesAvailable(context);

        if (result != ConnectionResult.SUCCESS) {
            return null;
        }*/

        if (tracker == null) {

            analytics = GoogleAnalytics.getInstance(context);
            analytics.setLocalDispatchPeriod(10);

            tracker = analytics.newTracker("UA-85535072-3"); // Replace with actual tracker id
            tracker.enableExceptionReporting(true);
            tracker.enableAdvertisingIdCollection(true);
            //  tracker.enableAutoActivityTracking(true);
            tracker.setScreenName(screenName);
            tracker.send(new HitBuilders.ScreenViewBuilder().build());

        } else {
            tracker.setScreenName(screenName);
            tracker.send(new HitBuilders.ScreenViewBuilder().build());

        }

        return tracker;
    }


}
