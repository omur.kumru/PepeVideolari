package com.oksin.video.pepevideolari;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;



public class FirstTab extends Fragment {

    ArrayList<Drawable> imageDrawableList;
    private ListView listView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViews();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.first_fragment, container, false);

        CustomAdapter adapter = new CustomAdapter(getContext(), StringUtils.videoChapterNames, StringUtils.videoDurationChapters, imageDrawableList);

        listView = (ListView) rootView.findViewById(R.id.list_fragment);
        listView.setAdapter(adapter);

        initEvents();

        return rootView;
    }


    private void initImage() throws IOException {

        Drawable uploadedDrawable = setImageFromAsset("sarkilar.png");

        imageDrawableList = new ArrayList<>();

        imageDrawableList.add(uploadedDrawable);

        for (int i = 0; i < StringUtils.pictureNames.size(); ++i) {
            Drawable generalDrawable = setImageFromAsset(StringUtils.pictureNames.get(i));
            imageDrawableList.add(generalDrawable);
        }

    }

    private Drawable setImageFromAsset(String videoName) throws IOException {

        InputStream inputStream = getActivity().getAssets().open(videoName);
        Drawable drawable = Drawable.createFromStream(inputStream, null);
        return drawable;
    }

    private void initViews() {

        try {
            initImage();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void initEvents() {

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                int i = 0;

                if (position == 0) {

                    Intent intent = new Intent(getContext(), VideoFirstActivity.class);
                    startActivity(intent);


                } else {

                    while (true) {

                        if (position == i + 1) {

                            Log.d("listeEleman", String.valueOf(position));
                            double videoSize = 0.0;
                            String newFileName;

                            if ((i + 7) < StringUtils.videoChapterNames.size()) {

                                videoSize = StringUtils.videoDownloadedByte[i + 7];
                                newFileName = StringUtils.videoURLChaptersName[i + 7];

                            } else {

                                newFileName = "arrayindexoutofbounds";

                            }

                            Intent intentGeneral = new Intent(getContext(), VideoActivity.class);
                            intentGeneral.putExtra("whichTab",true);
                            intentGeneral.putExtra("videoURL", StringUtils.videoURLChapters[i]);
                            intentGeneral.putExtra("whichVideo", StringUtils.videoURLChaptersName[i]);
                            intentGeneral.putExtra("videoSize", videoSize);
                            intentGeneral.putExtra("newFileName", newFileName);
                            intentGeneral.putExtra("screenName", StringUtils.screenNames[i]);
                            intentGeneral.putExtra("downloadSize", StringUtils.videoDownloadedByte[i]);

                            //downloadedVideo
                            intentGeneral.putExtra("downloadedVideoListName", StringUtils.videoChapterNames.get(i));
                            intentGeneral.putExtra("downloadedVideoListDuration", StringUtils.videoDurationChapters.get(i));
                            intentGeneral.putExtra("downloadedVideoImage", StringUtils.pictureNames.get(i));
                            intentGeneral.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            intentGeneral.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            //  finish();

                            startActivity(intentGeneral);

                            break;
                        } else {
                            i++;
                        }

                    }
                }
            }
        });

    }
}
