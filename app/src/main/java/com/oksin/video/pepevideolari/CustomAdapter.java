package com.oksin.video.pepevideolari;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;


public class CustomAdapter extends BaseAdapter {

    List<String> videoNames;
    List<String> videoDurations;
    Context context;
   // Drawable[] imageId;
    List<Drawable> imageId;
    private static LayoutInflater inflater = null;


    public CustomAdapter(Context context, List<String> videoNameList, List<String> videoDurationList, List<Drawable> images) {
        // TODO Auto-generated constructor stub
        videoNames = videoNameList;
        videoDurations = videoDurationList;
        this.context = context;
        imageId = images;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        return videoNames.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder
    {
        TextView textDuration;
        TextView textVideoName;
        ImageView videoImage;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder holder=new Holder();
        View rowView = inflater.inflate(R.layout.list_row, null);
        holder.textDuration = (TextView) rowView.findViewById(R.id.videoDuration);
        holder.textVideoName = (TextView) rowView.findViewById(R.id.videoName);
        holder.videoImage = (ImageView) rowView.findViewById(R.id.list_image);

        holder.textVideoName.setText(videoNames.get(position));
        holder.textDuration.setText(videoDurations.get(position));
        holder.videoImage.setImageDrawable(imageId.get(position));

        return rowView;
    }
}
