package com.oksin.video.pepevideolari;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.VideoView;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;


public class VideoFirstActivity extends AppCompatActivity implements EasyVideoCallback {

    private EasyVideoPlayer player;
    private static String screenName = "FirstScreen";

    //https://dl.dropboxusercontent.com/s/re9ysy6clhrp0l2/zuluyasasirti.mp4?dl=0
    //zulu

    InterstitialAd mInterstitialAd;

    MediaPlayer mp;

    VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Utils.getTracker(VideoFirstActivity.this, screenName);

        setContentView(R.layout.first_video);
        initAd();

        player = (EasyVideoPlayer) findViewById(R.id.player);
        player.setCallback(this);

        Uri videoUri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.pepesarkilar);


      /*  MediaController mediaController= new MediaController(this);
        mediaController.setAnchorView(videoView);

        videoView = (VideoView) findViewById(R.id.player);
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(videoUri);
        videoView.requestFocus();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                videoView.start();
            }
        });*/
     //   mp = MediaPlayer.create(VideoFirstActivity.this.getApplicationContext(), R.raw.pepesarkilar);

/*
        try {
            mp.setDataSource(VideoFirstActivity.this, videoUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
*/

  //      mp.reset();
   //     mp.prepareAsync();
  //      mp.start();

        player.setSource(videoUri);

        if(player.isPrepared()) {
            player.start();
        }

        //System.gc();

    }

    @Override
    protected void onPause() {
        super.onPause();

        player.pause(); //added  //Change

    }

    private void initAd() {

        mInterstitialAd = new InterstitialAd(VideoFirstActivity.this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3329588112236907/5222479676");


        AdRequest adRequestIn = new AdRequest.Builder()
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                //.addTestDevice("E8CEE1A5925198EC7B7DD03CC4BAD6B9")
                .setRequestAgent("android_studio:ad_template").build();

        mInterstitialAd.loadAd(adRequestIn);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        //change later  player.stop();

        initAd();
        player.stop();

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mInterstitialAd.show();
            }

            @Override
            public void onAdClosed() {
                super.onAdOpened();
                Intent intent = new Intent(VideoFirstActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
             //   finish();
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStarted(EasyVideoPlayer player) {

    }

    @Override
    public void onPaused(EasyVideoPlayer player) {

    }

    @Override
    public void onPreparing(EasyVideoPlayer player) {

    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {

        player.start();
    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {

    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {

    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {

    }
}
