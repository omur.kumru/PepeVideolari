package com.oksin.video.pepevideolari;

import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



public class StringUtils {

    public static ArrayList<Video> downloadedVideoList = new ArrayList<>();

    public static List<String> videoName = new ArrayList<>();
    public static List<String> videoDuration = new ArrayList<>();
    public static List<Drawable> videoDrawable = new ArrayList<>();


    public static List<String> pictureNames =  Arrays.asList("firstch.png", "secondch.png", "thirdch.png", "fourthch.png", "fifthch.png", "sixthch.png", "zuluhalay.png",
            "eightch.png", "ninthch.png", "tenthch.png", "eleventhch.png", "twelfthch.png", "thirteenthch.png", "fourteenthch.png", "fifteenthch.png",
            "sixteenthch.png", "seventeenthch.png", "18.png", "nineteentch.png", "twentiethch.png", "kuru.png", "agliyor.png", "zulu.png", "pepesaat.png", "pepekalbim.png", "pepesac.png", "sayma.png",
            "21.png", "22.png", "23.png", "24.png", "25.png", "26.png", "27.png", "28.png", "29.png", "30.png", "31.png", "32.png",
            "33.png", "34.png", "35.png", "36.png", "37.png", "38.png", "39.png", "40.png", "41.png", "42.png", "43.png", "44.png", "45.png", "46.png", "47.png", "48.png", "49.png",
            "50.png", "51.png", "52.png", "53.png", "54.png", "55.png", "56.png", "57.png", "58.png", "59.png", "60.png"

    );

    public static double[] videoDownloadedByte = new double[]{

            62.9, 69.4, 71.0, 72.3, 63.3, 69.9, 68.5, 60.3, 57.9, 69.3,
            66.0, 63.8, 65.9, 66.2, 74.1, 73.4, 63.7, 55.7, 67.7, 60.8,
            53.5, 59.3, 48.7, 276, 46.4, 28.1, 55.1,  //Aradaki 7 bölüm
            72.4, 64.6, 68.5, 71.7, 61.1, 70.4, 67.4, 71.1, 63.4, 57.6,
            69.2, 70.5, 68.5, 76.0, 60.0, 64.9, 71.4, 63.9, 66.1, 59.9,
            68.4, 65.0, 54.9, 68.5, 69.2, 59.4, 64.1, 66.0, 64.0, 57.7,
            59.4, 57.7, 59.8, 58.9, 58.2, 57.7, 61.6, 54.9, 55.6, 60.1

    };

    public static List<String> videoChapterNames = Arrays.asList(
            "Pepe Şarkıları\n13 Şarkı Bir Arada",
            "Zıtlıklar \nBüyük Küçük",      //bolum1
            "Önemli Olan \nOyun Oynamak",   //bolum2
            "Pepee Kırmızı\nArıyor",        //bolum3
            "Trakya Karşılaması",           //bolum4
            "Pepee Uçmak\nİstiyor",         //bolum5
            "Pepee Maymuşla\nTanışıyor",    //bolum6
            "Zulunun\nDoğumgünü Halayı",    //bolum7
            "Pepee'nin Beş\nSüprizi Var",   //bolum8
            "Pepee ve \n6 Kurbağa",         //bolum9
            "Hangisi Kaç\nTane Oyunu",      //bolum10
            "Benim Annem\nGüzel Annem",     //bolum11
            "Farklı Olanı\nBul Oyunu",      //bolum12
            "Pepee Saklambaç\nOynuyor",     //bolum13
            "Yaşasın\nYemek Yemek",         //bolum14
            "Sarıları\nBul Oyunu",          //bolum15
            "Pepee'nin\nMinik Burnu",       //bolum16
            "Pepee Tuvaletini\nSöylüyor",   //bolum17
            "Pepee\nKardeşini Seviyor",     //bolum18
            "Pepee'nin\nYükseklik Korkusu", //bolum19
            "Pepee\nMavi Arıyor",           //bolum20
            "Islak Kuru",
            "Pepee Ağlıyor",
            "Zulu'ya Şaşırtı",
            "Pepee İle \nDolu Dolu 1 Saat",
            "Kalbim Kırıldı",
            "Pepee Saçını \nKestiriyor",
            "Pepee Saymayı \nÖğreniyor",
            "Pepee İp\nAtlamayı Öğreniyor", //bolum21
            "10'a Kadar Sayma",             //bolum22
            "Hüdayda",                      //bolum23
            "Bisikletten\nDüşen Pepee",     //bolum24
            "Bebee'nin\nDoğum Günü",        //bolum25
            "Bebee'nin \nİlk Adımı",        //bolum26
            "Pepee'nin\nYeşilleri Çoğalıyor",//bolum27
            "Pepee\nDişlerini Fırçalıyor",  //bolum28
            "Duygu Organları\nKulak",       //bolum29
            "Pepee'nin Şekilleri",          //bolum30
            "Zıtlıklar\nUzunluk-Kısalık",   //bolum31
            "Yaşasın\nZıp Zıp Balonlar",    //bolum32
            "Pepee Uzak\nYakın Oynuyor",    //bolum33
            "Pepee\nHoron Tepiyor",         //bolum34
            "Sıcak-Soğuk",                  //bolum35
            "Pepee'nin\nKarnı Acıkmış",     //bolum36
            "Pepee\nDokunup Anlıyor",       //bolum37
            "Geceleri\nHerkes Uyur",        //bolum38
            "Aynı - Farklı",                //bolum39
            "Pepee\nMööcükle Tanışıyor",    //bolum40
            "Tadına Bayıldım",              //bolum41
            "Biz\nMeyveyi Seviyoruz",       //bolum42
            "Pepee\nToplamacılık Oynuyor",  //bolum43
            "Pepee Keşfe Çıkıyor",          //bolum44
            "Bebee Artık\nEmzik Emmeyecek", //bolum45
            "Zıtlıklar Az - Çok",           //bolum46
            "Baba Sevgisi",                 //bolum47
            "Kara Möcük\nKayboldu",         //bolum48
            "100'e Kadar\nSayma Oyunu",     //bolum49
            "Pepee\nAltıgeni Öğreniyor",    //bolum50
            "Pepee ve Bebee\nÇıkarma Öğreniyor",//bolum51
            "Pepee Atma\nKullan Öğreniyor", //bolum52
            "Çayda\nÇıra Oynuyor",          //bolum53
            "Bana Beni\nSevdiğini Söyle",   //bolum54
            "Susadım\nSu İsterim",          //bolum55
            "Yaz Geldi\nHava Çok Sıcak",    //bolum56
            "Önünde Arkanda",               //bolum57
            "En Uzundan\nEn Kısaya",        //bolum58
            "Pepee'nin\nDon Don Oyunu",     //bolum59
            "Bebee'nin Dişi\nHastalandı"    //bolum60

    );

    public static String[] videoURLChaptersName = new String[]{

            "bolum1.mp4", "bolum2.mp4", "bolum3.mp4", "bolum4.mp4", "bolum5.mp4", "bolum6.mp4",
            "zuluhalay.mp4", "bolum8.mp4", "bolum9.mp4", "bolum10.mp4", "bolum11.mp4", "bolum12.mp4", "bolum13.mp4", "bolum14.mp4", "bolum15.mp4",
            "bolum16.mp4", "bolum17.mp4", "kardesiniseviyor.mp4", "bolum19.mp4", "bolum20.mp4", "islakkuru.mp4", "pepeagliyor.mp4", "pepezulu.mp4", "doludolusaat.mp4",
            "kalbimkirildi.mp4", "sacinikestiriyor.mp4", "pepesayma.mp4", "bolum21.mp4", "bolum22.mp4", "bolum23.mp4", "bolum24.mp4", "bolum25.mp4", "bolum26.mp4", "bolum27.mp4",
            "bolum28.mp4", "bolum29.mp4", "bolum30.mp4", "bolum31.mp4", "bolum32.mp4", "bolum33.mp4", "bolum34.mp4", "bolum35.mp4", "bolum36.mp4", "bolum37.mp4", "bolum38.mp4", "bolum39.mp4",
            "bolum40.mp4", "bolum41.mp4", "bolum42.mp4", "bolum43.mp4", "bolum44.mp4", "bolum45.mp4", "bolum46.mp4", "bolum47.mp4", "bolum48.mp4", "bolum49.mp4", "bolum50.mp4", "bolum51.mp4",
            "bolum52.mp4", "bolum53.mp4", "bolum54.mp4", "bolum55.mp4", "bolum56.mp4", "bolum57.mp4", "bolum58.mp4", "bolum59.mp4", "bolum60.mp4"
    };

    public static List<String> videoDurationChapters = Arrays.asList(

            "16:19", "11:09", "11:09", "11:13", "11:11", "11:13", "11:09", "11:16", "11:16", "11:05", "11:45", "11:33", "11:16", "11:25",
            "11:24", "11:21", "11:31", "11:14", "11:09", "11:14", "11:25", "10:42", "10:10", "10:47", "60:00", "10:35", "10:03", "10:49",
            "11:25", "11:56", "11:24", "11:24", "11:16", "11:16", "11:28", "11:20", "11:28", "11:33", "11:48", "11:24", "11:17", "11:30", "11:56", "11:21", "11:23", "12:15", "11:29", "11:27",
            "11:29", "11:17", "11:19", "11:34", "11:26", "11:23", "12:17", "11:47", "11:37", "11:32", "11:22", "11:51", "11:41", "11:53", "10:42", "11:46", "12:08", "11:29", "11:29", "12:06"

    );


    public static String[] videoURLChapters = new String[]{

            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!113&authkey=!ACDk-Gl7aHFdRt4&ithint=video%2cmp4",    //1
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!114&authkey=!AGk_V8kexCqRPpw&ithint=video%2cmp4",    //2
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!115&authkey=!AArIYfgYaQXuGWc&ithint=video%2cmp4",    //3
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!118&authkey=!AIdmqLWqV1nfAA4&ithint=video%2cmp4",    //4
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!116&authkey=!AIMbhoDWjDi46ek&ithint=video%2cmp4",    //5
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!117&authkey=!ADiLc8lYz-XGdWw&ithint=video%2cmp4",    //6
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!120&authkey=!AMr9u0QJXgqfDrY&ithint=video%2cmp4",    //7
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!119&authkey=!AKsUdUHacNt4Aok&ithint=video%2cmp4",    //8
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!121&authkey=!ABb-VPxORZ70T7U&ithint=video%2cmp4",    //9
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!122&authkey=!AAGvEcJF8vNlVvE&ithint=video%2cmp4",    //10
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!123&authkey=!ALgcnHvyk8h8K-8&ithint=video%2cmp4",    //11
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!124&authkey=!ANXFLe7zZAbe6n0&ithint=video%2cmp4",    //12
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!126&authkey=!APHA8hQ3AD4IOho&ithint=video%2cmp4",    //13
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!125&authkey=!AOK0GpTAxVEXTP8&ithint=video%2cmp4",    //14
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!127&authkey=!ADVLbwJ4mcNw6dY&ithint=video%2cmp4",    //15
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!129&authkey=!AM1cOmEyld2xG2k&ithint=video%2cmp4",    //16
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!130&authkey=!AFK6Co08R1uAZR8&ithint=video%2cmp4",    //17
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!128&authkey=!AH3KVJrhe28IF-I&ithint=video%2cmp4",    //18
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!128&authkey=!AH3KVJrhe28IF-I&ithint=video%2cmp4",    //19
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!131&authkey=!AKKJt9q-H52GthU&ithint=video%2cmp4",    //20

            //ıslak kuru
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!301&authkey=!ADdai4c2eSCgs3U&ithint=video%2cmp4",
            //pepe ağlıyor
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!302&authkey=!AKgc0jDi7-wbbfU&ithint=video%2cmp4",
            //zuluya şaşırtı
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!306&authkey=!AIhNqcB0grPC0YU&ithint=video%2cmp4",
            //pepe ile dolu dolu 1 saat
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!307&authkey=!AHCsSmcXsuJrmEQ&ithint=video%2cmp4",
            //kalbim kırıldı
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!304&authkey=!AJUfzgb2ZS28Z24&ithint=video%2cmp4",
            //pepe saçını kestiriyor
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!303&authkey=!AH0WPIR3GHtu4a0&ithint=video%2cmp4",
            //pepe saymayı öğreniyor
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!305&authkey=!AHd-7K_1ALha2jw&ithint=video%2cmp4",


            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!133&authkey=!ALdVn6HEY_sCPbo&ithint=video%2cmp4",    //21 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!134&authkey=!AFEuVwRswwxyfU4&ithint=video%2cmp4",    //22
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!135&authkey=!AEKrYluBmHugQaE&ithint=video%2cmp4",    //23
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!136&authkey=!AK9oik6EwwO-Ujs&ithint=video%2cmp4",    //24
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!137&authkey=!AE2yucfMbFFZB_U&ithint=video%2cmp4",    //25
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!138&authkey=!ADQNHBqwICgNG2U&ithint=video%2cmp4",    //26
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!139&authkey=!AG9P183QtnvM6NQ&ithint=video%2cmp4",    //27
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!140&authkey=!AND0Z9yNTuHABuU&ithint=video%2cmp4",    //28
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!141&authkey=!AE3NwJiwoXCI9NU&ithint=video%2cmp4",    //29
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!142&authkey=!AJTelkAUoEnae3k&ithint=video%2cmp4",    //30
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!143&authkey=!AFwEJQ8fkt_4XIQ&ithint=video%2cmp4",    //31 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!144&authkey=!AAftZ4TC0Wvgjoo&ithint=video%2cmp4",    //32 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!145&authkey=!AKAVQ0ynsTZhIEQ&ithint=video%2cmp4",    //33
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!146&authkey=!ABor2cQuYOS_Zl0&ithint=video%2cmp4",    //34 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!147&authkey=!ABc_imoVOodBkHE&ithint=video%2cmp4",    //35 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!148&authkey=!ANF6vJ3J6ZDXo10&ithint=video%2cmp4",    //36
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!149&authkey=!AHJXhm1be-FMXDM&ithint=video%2cmp4",    //37
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!150&authkey=!APHSvanb-4eC7-U&ithint=video%2cmp4",    //38
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!151&authkey=!AFaQAdP9bjaMcSw&ithint=video%2cmp4",    //39 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!152&authkey=!ABYd7muBJId4cvQ&ithint=video%2cmp4",    //40
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!153&authkey=!AOn9WJx8VQh-gW4&ithint=video%2cmp4",    //41 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!154&authkey=!ANAZdugH3bAZ6aY&ithint=video%2cmp4",    //42
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!155&authkey=!AOuTsfFdWFgiAm8&ithint=video%2cmp4",    //43 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!156&authkey=!ALJVI1j0KO6a2bU&ithint=video%2cmp4",    //44
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!157&authkey=!AIUnQ4QeopOLtvM&ithint=video%2cmp4",    //45
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!158&authkey=!AIYTTyYScqTONjI&ithint=video%2cmp4",    //46
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!159&authkey=!AM_fqoSsdFVlvtg&ithint=video%2cmp4",    //47
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!160&authkey=!AKqPDNS0JwD9bDg&ithint=video%2cmp4",    //48 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!161&authkey=!AF-qyTz5cO1lvsc&ithint=video%2cmp4",    //49
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!162&authkey=!APgFcgbU-jIn8_g&ithint=video%2cmp4",    //50 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!163&authkey=!AB4oPUZLEfktruU&ithint=video%2cmp4",    //51
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!164&authkey=!AETlZLNnqNs_7ag&ithint=video%2cmp4",    //52
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!165&authkey=!AKl3HSuuVNHjJcc&ithint=video%2cmp4",    //53 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!166&authkey=!AAB93AvIsdzURWI&ithint=video%2cmp4",    //54
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!167&authkey=!APdZW_SRo1Gwyv0&ithint=video%2cmp4",    //55 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!168&authkey=!AEyvCGW-pSeJeY4&ithint=video%2cmp4",    //56 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!169&authkey=!AA9g2Qyw5L78X8o&ithint=video%2cmp4",    //57
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!170&authkey=!ALwr-nncmKH-nrQ&ithint=video%2cmp4",    //58 ?
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!171&authkey=!AG0YDLIM-t7-E48&ithint=video%2cmp4",    //59
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!172&authkey=!AOkYRcoQLOnW70U&ithint=video%2cmp4"     //60 ?

    };

    public static String[] screenNames = new String[]{"FirstScreen", "SecondScreen", "ThirdScreen", "FourthScreen", "FifthScreen", "SixthScreen", "SeventhScreen", "EightScreen", "NinthScreen", "TenthScreen"
            , "EleventhScreen", "TwelfthScreen", "ThirteenthScreen", "FourteenthScreen", "FifteenthScreen", "SixteenthScreen", "EighteenthScreen", "NineteenthScreen", "TwentiethScreen",
            "21Screen", "22Screen", "23Screen", "24Screen", "25Screen", "26Screen", "27Screen", "28Screen", "26Screen", "27Screen", "28Screen", "29Screen", "30Screen", "31Screen", "32Screen", "33Screen", "34Screen",
            "35Screen", "36Screen", "37Screen", "38Screen", "39Screen", "40Screen", "41Screen", "42Screen", "43Screen", "44Screen", "45Screen", "46Screen", "47Screen", "48Screen", "49Screen", "50Screen",
            "51Screen", "52Screen", "53Screen", "54Screen", "55Screen", "56Screen", "57Screen", "58Screen", "59Screen", "60Screen", "61Screen", "62Screen", "63Screen", "64Screen", "65Screen", "66Screen"
    };


    //Second Tab *****************************************************************************************************************

    public static String[] videoURLChaptersSecond = new String[]{

            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!539&authkey=!ADKB1pmpFNkzh4s&ithint=video%2cmp4",    //1
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!538&authkey=!AMK2Xrge8SV2QR4&ithint=video%2cmp4",    //2
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!537&authkey=!ANCZvqGDbAsHhS0&ithint=video%2cmp4",    //3
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!541&authkey=!AMNZbnipdzTFcds&ithint=video%2cmp4",    //4
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!540&authkey=!AKCTl_aCt5slcqo&ithint=video%2cmp4",    //5
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!542&authkey=!ADAoZa9D80UdFKc&ithint=video%2cmp4",    //6
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!543&authkey=!AKORRRkUa9FbRXQ&ithint=video%2cmp4",    //7
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!544&authkey=!AInxJM9q0ztEBhI&ithint=video%2cmp4",    //8
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!545&authkey=!AFBpb4oVcB9uiEQ&ithint=video%2cmp4",    //9
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!546&authkey=!AIKvOUmrexj0k5U&ithint=video%2cmp4",    //10
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!584&authkey=!AJTVKNJRBC_sWjA&ithint=video%2cmp4",    //11
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!547&authkey=!AL-2Z3HoE84OfXs&ithint=video%2cmp4",    //!2
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!548&authkey=!AE72A9sv96FgEPE&ithint=video%2cmp4",    //13
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!549&authkey=!ABtET-swqLnxK8M&ithint=video%2cmp4",    //14
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!550&authkey=!AMdAQrnVW-QP5lw&ithint=video%2cmp4",    //15
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!551&authkey=!ALFGjT66H3hPCoI&ithint=video%2cmp4",    //16
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!553&authkey=!ADIfj2o5-rGIsxA&ithint=video%2cmp4",    //17
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!552&authkey=!ANAKO_MOJxbrJLM&ithint=video%2cmp4",    //18
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!554&authkey=!AB9HAscZvFb8Qas&ithint=video%2cmp4",    //19
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!555&authkey=!AGvYNtqnIGWl5-o&ithint=video%2cmp4",    //20
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!556&authkey=!AIleNyiTZOfQOlU&ithint=video%2cmp4",    //21
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!557&authkey=!AAv6E2lwnL9Kz5E&ithint=video%2cmp4",    //22
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!558&authkey=!AOLAiO6Slkizn3U&ithint=video%2cmp4",    //23
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!559&authkey=!ADQ9z7huWxUFaM0&ithint=video%2cmp4",    //24
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!560&authkey=!AJkKJ5LNdXbx1pk&ithint=video%2cmp4",    //25
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!561&authkey=!ANJaXIzDrk3Z72Y&ithint=video%2cmp4",    //26
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!562&authkey=!AB6PGcZ4ExEfEUw&ithint=video%2cmp4",    //27
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!563&authkey=!AJwub0x_Jb7FBuM&ithint=video%2cmp4",    //28
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!564&authkey=!AP7Zno9ffD3A_n8&ithint=video%2cmp4",    //29
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!565&authkey=!ALPRKp2v8CDtuTY&ithint=video%2cmp4",    //30
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!566&authkey=!ADWTYXpZYxmzPM8&ithint=video%2cmp4",    //31
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!567&authkey=!AHlclri3i5zkJLw&ithint=video%2cmp4",    //32
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!568&authkey=!AFEDjikxpSCbpBw&ithint=video%2cmp4",    //33 - 34
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!569&authkey=!ACFuos7tzWLrbRU&ithint=video%2cmp4",    //34
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!570&authkey=!AAuhTXt1eJVj6rg&ithint=video%2cmp4",    //35
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!572&authkey=!ADnzD7YuHkvIq8I&ithint=video%2cmp4",    //36
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!573&authkey=!AOo8r30knqV6IVs&ithint=video%2cmp4",    //37
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!574&authkey=!AKzwyIEqiu7Yhpg&ithint=video%2cmp4",    //38
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!575&authkey=!APZUj6unoS8qU-c&ithint=video%2cmp4",    //39
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!576&authkey=!AHF-c8mIu1FmBjI&ithint=video%2cmp4",    //40
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!577&authkey=!AI1cYUAdstkz1dQ&ithint=video%2cmp4",    //41
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!583&authkey=!AP-B4Zyx1b838LA&ithint=video%2cmp4",    //42
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!578&authkey=!AJ-Br74KeV2dnoM&ithint=video%2cmp4",    //43
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!579&authkey=!APPoRMN4NIWxx1w&ithint=video%2cmp4",    //44
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!580&authkey=!AErP6Z9_CcbXkUs&ithint=video%2cmp4",    //45
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!581&authkey=!AJd6X2tx6NnV6_w&ithint=video%2cmp4",    //46
            "https://onedrive.live.com/download?resid=8FAF94680E88CDC2!582&authkey=!AK6_D7SBtzd2RKk&ithint=video%2cmp4",    //47

    };

    public static String[] screenNamesSecondTab = new String[]{
            "1Screen", "2Screen", "3Screen", "4Screen", "5Screen", "6Screen", "7Screen", "8Screen", "9Screen", "10Screen"
            , "11Screen", "12Screen", "13Screen", "14Screen", "15Screen", "16Screen", "17Screen", "18Screen", "19Screen", "20Screen",
            "21Screen", "22Screen", "23Screen", "24Screen", "25Screen", "26Screen", "27Screen", "28Screen", "26Screen", "27Screen", "28Screen", "29Screen", "30Screen", "31Screen", "32Screen", "33Screen", "34Screen",
            "35Screen", "36Screen", "37Screen", "38Screen", "39Screen", "40Screen", "41Screen", "42Screen", "43Screen", "44Screen", "45Screen", "46Screen", "47Screen", "48Screen"
    };

    public static List<String> videoChapterNamesSecondTab = Arrays.asList(

            "Büyümek Güzeldir",             //bolum1
            "Kalbim Kırıldı",               //bolum2
            "Yaşasın Farklılıklar",         //bolum3
            "Pepenin Yeni\nArkadaşı Zeze",  //bolum4
            "Pepee Okulllu\nOluyor",        //bolum5
            "Özledim Seni",                 //bolum6
            "Hayde Bre\nPehlivan",          //bolum7
            "Bugün 23 Nisan",               //bolum8
            "8 Saatlik\nSabır Oyunu",       //bolum9
            "Şuut ve Basket",               //bolum10
            "Çıt Çıt\nKeseriz",             //bolum11
            "Denizde Yüzmek",               //bolum12
            "Seviyorsan Söyle",             //bolum13
            "Göz Kulak Olmak",              //bolum14
            "Korsan Pepe",                  //bolum15
            "Balerin Duduu",                //bolum16
            "Maymuşun Dişleri",             //bolum17
            "Affet Beni",                   //bolum18
            "Sevgili Prenses",              //bolum19
            "Biraz Hızlı\nBiraz Yavaş",     //bolum20
            "Uyumak Çok\nGüzeldir",         //bolum21
            "Hayvanlar\nDostumuzdur",       //bolum22
            "Bir Kap Su\nBiraz Yemek",      //bolum23
            "Ekee'nin Saçları",             //bolum24
            "Sevgili Korsan",               //bolum25
            "Seni Seviyorum",               //bolum26
            "Sabır Sabır\nYa Sabır",        //bolum27
            "Şut ve Gol\nİşte Futbol",      //bolum28
            "Asıl Küreklere",               //bolum29
            "Büyük mü?\nKüçük mü?",         //bolum30
            "Balıkçı Pepee",                //bolum31
            "Bibii Eke'yi\nÜzünce",         //bolum32
            "Bibii'ye Şaşırtı",             //bolum33
            "Meyve Şenliği",                //bolum34
            "Sevgili Balıkçı",              //bolum35
            "Sevgili Kaptan",               //bolum36
            "Sevgili Doktor",               //bolum37
            "Paylaşmak Güzeldir",           //bolum38
            "Zuluya Şaşırtı",               //bolum39
            "Kendi Kendine",                //bolum40
            "Yaşasın Oyun\nOynamak",        //bolum41
            "Ay Ay Ay",                     //bolum42
            "Islak Kuru",                   //bolum43
            "Her Yer\nBembeyaz",            //bolum44
            "Dünya'yı\n Keşfet",            //bolum45
            "Yuma Yuma\nYumurta",           //bolum46
            "Evde Sinema\nGecesi"           //bolum47

    );

    public static List<String> videoDurationChaptersSecondTab = Arrays.asList(

            "10:31", "10:35", "10:17", "10:26", "10:25", "10:24", "10:26", "10:16", "10:29", "10:15", "10:20", "10:20", "10:28", "10:20",
            "10:20", "10:23", "10:24", "10:46", "10:26", "10:14", "10:21", "10:20", "10:22", "10:18", "10:22", "10:26", "10:37", "10:39",
            "10:37", "10:48", "10:38", "10:39", "10:36", "10:42", "10:44", "10:48", "10:42", "10:48", "10:48", "10:44", "10:35", "10:48",
            "10:42", "10:44", "10:57", "10:42", "10:42"


    );

    public static List<String> videoURLChaptersNameSecondTab = Arrays.asList(

            "bolum1s.mp4", "bolum2s.mp4", "bolum3s.mp4", "bolum4s.mp4", "bolum5s.mp4", "bolum6s.mp4", "bolum7s.mp4", "bolum8s.mp4", "bolum9s.mp4", "bolum10s.mp4",
            "bolum11s.mp4", "bolum12s.mp4", "bolum13s.mp4", "bolum14s.mp4", "bolum15s.mp4", "bolum16s.mp4", "bolum17s.mp4", "bolum18s.mp4", "bolum19s.mp4", "bolum20.smp4",
            "bolum21s.mp4", "bolum22s.mp4", "bolum23s.mp4", "bolum24s.mp4", "bolum25s.mp4", "bolum26s.mp4", "bolum27s.mp4", "bolum28s.mp4", "bolum29s.mp4", "bolum30s.mp4",
            "bolum31s.mp4", "bolum32s.mp4", "bolum33s.mp4", "bolum34s.mp4", "bolum35s.mp4", "bolum36s.mp4", "bolum37s.mp4", "bolum38s.mp4", "bolum39s.mp4",
            "bolum40s.mp4", "bolum41s.mp4", "bolum42s.mp4", "bolum43s.mp4", "bolum44s.mp4", "bolum45s.mp4", "bolum46s.mp4", "bolum47s.mp4", "bolum48s.mp4"

    );

    public static List<String> pictureNamesSecondTab = Arrays.asList(
            "1s.png", "2s.png", "3s.png", "4s.png", "5s.png", "6s.png", "7s.png",
            "8s.png", "9s.png", "10s.png", "11s.png", "12s.png", "13s.png", "14s.png", "15s.png",
            "16s.png", "17s.png", "18s.png", "19s.png", "20s.png", "21s.png", "22s.png", "23s.png", "24s.png", "25s.png", "26s.png", "27s.png",
            "28s.png", "29s.png", "30s.png", "31s.png", "32s.png",
            "33s.png", "34s.png", "35s.png", "36s.png", "37s.png", "38s.png", "39s.png", "40s.png", "41s.png", "42s.png", "43s.png", "44s.png", "45s.png", "46s.png", "47s.png", "48s.png"

    );


}
