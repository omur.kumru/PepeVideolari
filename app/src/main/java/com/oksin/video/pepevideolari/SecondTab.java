package com.oksin.video.pepevideolari;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;



public class SecondTab extends Fragment {

    ArrayList<Drawable> imageDrawableList;
    private ListView listView;

    public SecondTab() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViews();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.first_fragment, container, false);

        //changed                               //changed
        CustomAdapter adapter = new CustomAdapter(getContext(), StringUtils.videoChapterNamesSecondTab, StringUtils.videoDurationChaptersSecondTab, imageDrawableList);

        listView = (ListView) rootView.findViewById(R.id.list_fragment);
        listView.setAdapter(adapter);

        initEvents();

        return rootView;
    }

    private void initImage() throws IOException {

        imageDrawableList = new ArrayList<>();

        for (int i = 0; i < StringUtils.pictureNamesSecondTab.size(); ++i) {
            Drawable generalDrawable = setImageFromAsset(StringUtils.pictureNamesSecondTab.get(i));
            imageDrawableList.add(generalDrawable);
        }

    }

    private Drawable setImageFromAsset(String videoName) throws IOException {

        InputStream inputStream = getActivity().getAssets().open(videoName);
        Drawable drawable = Drawable.createFromStream(inputStream, null);
        return drawable;
    }


    private void initViews() {

        try {
            initImage();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void initEvents() {

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {

                int i = position;

                Intent intentGeneral = new Intent(getContext(), VideoActivity.class);
                intentGeneral.putExtra("whichTab", false);
                intentGeneral.putExtra("videoURL", StringUtils.videoURLChaptersSecond[i]);          //changed
                intentGeneral.putExtra("whichVideo", StringUtils.videoURLChaptersNameSecondTab.get(i)); //changed
                intentGeneral.putExtra("screenName", StringUtils.screenNamesSecondTab[i]);          //changed
                //       intentGeneral.putExtra("downloadSize", StringUtils.videoDownloadedByte[i]);  ???
                intentGeneral.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intentGeneral.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //  finish();

                startActivity(intentGeneral);


            }
        });

    }
}