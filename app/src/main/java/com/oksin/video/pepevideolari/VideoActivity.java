package com.oksin.video.pepevideolari;

import android.Manifest;
import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;


public class VideoActivity extends AppCompatActivity implements EasyVideoCallback {

    private EasyVideoPlayer player;
    Button downloadButton;
    File downloadedFile;
    File downloadedFileApp;
    File file;

    String whichVideo;
    String newFileName;
    double videoSize;
    String videoURL;
    String screenName;
    boolean whichTab;
    double downloadSize;

    File directoryFile;
    boolean timeFlag;
    InterstitialAd mInterstitialAd;
    ProgressDialog progressBarDialog;
    private BroadcastReceiver sendBroadcastReceiver;

    boolean downloadSuccess;
    IntentFilter intentFilter;
    IntentFilter intentFilterInternet;
    AlertDialog.Builder alertBuilder;
    AlertDialog networkAlert;
//_data /storage/emulated/0/Android/data/com.oksin.video.pepevideolari/files/data/user/0/com.oksin.video.pepevideolari/app_Pepe Videolari/bolum4.mp4


    //downloadedVideo
    String downloadedVideoName;
    String downloadedVideoDuration;
    String downloadImageName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViews();
        initFile();
        initBroadcastReceiver();
        initEvents();

        System.gc();

    }

    @Override
    protected void onResume() {
        super.onResume();

        checkIsDownloaded();

    }

    @Override
    protected void onPause() {
        super.onPause();
        player.pause(); //added
        checkIsDownloaded();
        closeDialog();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        player.reset();
        player.stop();
        // closeFile(downloadedFileApp);
        unregisterReceiver(sendBroadcastReceiver);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        initAd();
        player.stop();

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                mInterstitialAd.show();
            }

            @Override
            public void onAdClosed() {
                super.onAdOpened();
                Intent intent = new Intent(VideoActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                VideoActivity.this.overridePendingTransition(0, 0);

                VideoActivity.this.finish();
            }
        });

    }

    private void initViews () {

        Bundle extras = getIntent().getExtras();

        this.whichVideo = extras.getString("whichTab");
        this.videoURL = extras.getString("videoURL");
        this.whichVideo = extras.getString("whichVideo");
        this.screenName = extras.getString("screenName");
        this.downloadedVideoName = extras.getString("downloadedVideoListName");
        this.downloadedVideoDuration = extras.getString("downloadedVideoListDuration");
        this.downloadImageName = extras.getString("downloadedVideoImage");

        if (whichTab == true) {

            this.newFileName = extras.getString("newFileName");
            this.videoSize = extras.getDouble("videoSize");
            this.downloadSize = extras.getDouble("downloadSize");

        }

        Log.d("HangiBolum", whichVideo);
        Log.d("DownloadSizeGet", String.valueOf(downloadSize));

        //    this.timeFlag = extras.getBoolean("timeFlag");
        timeFlag = true;

        Utils.getTracker(VideoActivity.this, screenName);

        setContentView(R.layout.video_common);

        downloadButton = (Button) findViewById(R.id.downloadButton3);
        downloadButton.bringToFront();
        player = (EasyVideoPlayer) findViewById(R.id.player);
        assert player != null;
        player.setCallback(this);

    }

    private void initFile() {

        file = new File(Environment.getExternalStorageDirectory() + "/Pepe Videoları");
        ContextWrapper cw = new ContextWrapper(VideoActivity.this);

        directoryFile = cw.getDir("Pepe_Videolari", Context.MODE_PRIVATE);
        String fileName = directoryFile.getAbsolutePath() + "/" + whichVideo;

        downloadedFileApp = new File(fileName);
        if (downloadedFileApp != null) {
            // fileInApp = new File(downloadedFileApp.getAbsolutePath() + whichVideo);
            try {
                checkFileExist();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (!file.exists()) {

            file.mkdirs();

        } else {

            try {
                checkFileExist();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    private void initBroadcastReceiver() {

        intentFilter = new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE);
        intentFilterInternet = new IntentFilter();
        intentFilterInternet.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        sendBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(DownloadManager.ACTION_DOWNLOAD_COMPLETE)) {

                    try {

                        checkFileExist();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {

                    if (!isNetworkConnected()) {
                        //
                        showInternetDialog();
                        Log.d("networkConnectionVideo", "networkConnectionVideoOff");
                    } else {

                        if (networkAlert != null) {

                            networkAlert.cancel();
                        }
                    }
                }
            }
        };

    }

    private void initEvents() {

        downloadButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (askPermissionStorage() == true) {

                    if (isNetworkConnected() == true) {

                        try {

                            file_download(videoURL, VideoActivity.this, VideoActivity.this, whichVideo);

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                    } else {

                        LayoutInflater inflater = getLayoutInflater();
                        View viewTitle = inflater.inflate(R.layout.titlebar, null);

                        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(VideoActivity.this, R.style.AlertDialogCustom));
                        alertBuilder.setCancelable(true);
                        alertBuilder.setCustomTitle(viewTitle);
                        alertBuilder.setMessage("İnternet mevcut değil. Bölümleri telefonunuza kaydedebilmeniz için internetinizi açmanız gerekmektedir.");

                        AlertDialog alert = alertBuilder.create();
                        alert.show();

                    }

                    downloadButton.setEnabled(false);
                }

            }
        });
    }

    private void initAd() {

        mInterstitialAd = new InterstitialAd(VideoActivity.this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3329588112236907/5222479676");

        AdRequest adRequestIn = new AdRequest.Builder()
                //.addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                //.addTestDevice("E8CEE1A5925198EC7B7DD03CC4BAD6B9")
                .setRequestAgent("android_studio:ad_template").build();

        mInterstitialAd.loadAd(adRequestIn);

    }


    boolean file_download(final String uRl, final Context context, final Activity activity, final String whihcVideo) throws FileNotFoundException {

        progressBarDialog = new ProgressDialog(context);
        progressBarDialog.setTitle("Bölüm indiriliyor, lütfen bekleyin...");

        progressBarDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBarDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });

        progressBarDialog.setProgress(0);

        final Uri downloadUri = Uri.parse(uRl);

        ContextWrapper cw = new ContextWrapper(VideoActivity.this);

        directoryFile = cw.getDir("Pepe_Videolari", Context.MODE_PRIVATE);

        new Thread(new Runnable() {

            protected long downloadId;

            @Override
            public void run() {


                boolean downloading = true;

                DownloadManager manager = (DownloadManager) activity.getSystemService(Context.DOWNLOAD_SERVICE);
                DownloadManager.Request request = new DownloadManager.Request(downloadUri);
                // request.setDestinationInExternalPublicDir("/Pepe Videoları", whihcVideo);
                request.setDestinationInExternalFilesDir(context, directoryFile.getAbsolutePath(), whihcVideo);
                // request.addRequestHeader("Access-Control-Allow-Origin", "*");

                downloadId = manager.enqueue(request);
                while (downloading) {

                    DownloadManager.Query q = new DownloadManager.Query();

                    q.setFilterById(downloadId); //filter by id which you have receieved when reqesting download from download manager

                    Cursor cursor = manager.query(q);
                    cursor.moveToFirst();
                    int bytes_downloaded = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR));
                    int bytes_total = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES));

                    int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));

                    if (status == DownloadManager.STATUS_SUCCESSFUL) {
                        downloading = false;
                        downloadSuccess = true;
                        progressBarDialog.dismiss();
                    } else if (status == DownloadManager.STATUS_FAILED) {
                        downloadSuccess = false;
                    }

                    final int dl_progress = (int) ((bytes_downloaded * 100l) / bytes_total);  //get error here

                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            progressBarDialog.setProgress((int) dl_progress);

                        }
                    });

                    cursor.close();
                }

            }
        }).start();

        progressBarDialog.show();

        return downloadSuccess;

    }


    private void closeDialog() {
        if (progressBarDialog != null && progressBarDialog.isShowing()) {
            try {
                progressBarDialog.dismiss();
            } catch (IllegalArgumentException e) {
                // do nothing
            }
        }
    }

    public void checkIsDownloaded() {

        registerReceiver(sendBroadcastReceiver, intentFilter);
        registerReceiver(sendBroadcastReceiver, intentFilterInternet);

    }


    private double convetByteToKB(double downloadedFileSize) {

        double convert = 1048576;

        double temp = (downloadedFileSize / convert);

        double finalValue = Math.round(temp * 10.0) / 10.0;
        double result;

        if (finalValue % 1 == 0) {
            result = (int) finalValue;
        } else {
            result = finalValue;
        }

        return result;

    }


    private void checkFileExist() throws IOException {

        downloadedFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Pepe Videoları/" + whichVideo);

        ContextWrapper cw = new ContextWrapper(VideoActivity.this);

        directoryFile = cw.getDir("Pepe_Videolari", Context.MODE_PRIVATE);

        String res = getExternalFilesDir(directoryFile.getAbsolutePath()).toString() + "/" + whichVideo;

        downloadedFileApp = new File(res);

        if (downloadedFile.exists()) {

            downloadButton.setVisibility(View.GONE);

            Uri videoUri = Uri.fromFile(downloadedFile);

            player.reset();
            player.setSource(videoUri);

            if (player.isPrepared()) {

                player.start();

            } else if (player.isPlaying()) {
                player.pause();
            }

        } else if (downloadedFileApp.exists()) {

            downloadButton.setVisibility(View.GONE);

            double downloadFileSize = convetByteToKB(downloadedFileApp.length());

            if (newFileName != null) {

                if (newFileName.equals("arrayindexoutofbounds") || downloadFileSize == downloadSize) {

                    Uri videoUri = Uri.fromFile(downloadedFileApp);

                    player.reset();
                    player.setSource(videoUri);

                    if (player.isPrepared()) {

                        player.start();

                    } else if (player.isPlaying()) {

                        player.pause();
                    }

                } else if (downloadFileSize != downloadSize) {

                    if (downloadFileSize == videoSize) {

                        String fileName = getExternalFilesDir(directoryFile.getAbsolutePath()).toString() + "/" + whichVideo;

                        File from = new File(fileName);
                        from.setWritable(true);

                        String newFileNameFile = getExternalFilesDir(directoryFile.getAbsolutePath()).toString() + "/" + newFileName;

                        File to = new File(newFileNameFile);
                        to.setWritable(true);

                        if (from.exists()) {

                            from.renameTo(to);
                            Log.d("renameSuccessful", "renameToSuccessfull");

                        }

                        Uri videoUri = Uri.fromFile(from);

                        player.reset();
                        player.setSource(videoUri);

                    } else {

                        downloadedFileApp.delete();
                        downloadButton.setVisibility(View.VISIBLE);

                    }
                }
            } else {

                if (downloadFileSize < 5.0) {

                    downloadedFileApp.delete();
                    downloadButton.setVisibility(View.VISIBLE);

                }

                Drawable drawable = setImageFromAsset(downloadImageName);

                StringUtils.videoName.add(downloadedVideoName);
                StringUtils.videoDuration.add(downloadedVideoDuration);
                StringUtils.videoDrawable.add(drawable);

                Uri videoUri = Uri.fromFile(downloadedFileApp);

                player.reset();
                player.setSource(videoUri);

                if (player.isPrepared()) {

                    player.start();

                } else if (player.isPlaying()) {

                    player.pause();
                }

            }

        }
    }


    @Override
    public void onStarted(EasyVideoPlayer player) {


    }

    @Override
    public void onPaused(EasyVideoPlayer player) {


    }

    @Override
    public void onPreparing(EasyVideoPlayer player) {

    }

    @Override
    public void onPrepared(EasyVideoPlayer player) {


    }

    @Override
    public void onBuffering(int percent) {

    }

    @Override
    public void onError(EasyVideoPlayer player, Exception e) {

    }

    @Override
    public void onCompletion(EasyVideoPlayer player) {

    }

    @Override
    public void onRetry(EasyVideoPlayer player, Uri source) {

    }

    @Override
    public void onSubmit(EasyVideoPlayer player, Uri source) {

    }

    private boolean askPermissionStorage() {

        if (ContextCompat.checkSelfPermission(VideoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(VideoActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                ActivityCompat.requestPermissions(VideoActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

            } else {

                ActivityCompat.requestPermissions(VideoActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);

            }

            return false;
        }
        return true;
    }

    private boolean isNetworkConnected() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    public void showInternetDialog() {

        LayoutInflater inflater = getLayoutInflater();
        View viewTitle = inflater.inflate(R.layout.titlebar, null);

        alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(VideoActivity.this, R.style.AlertDialogCustom));
        alertBuilder.setCancelable(true);
        alertBuilder.setCustomTitle(viewTitle);
        alertBuilder.setMessage("İnternet mevcut değil. Bölümleri telefonunuza kaydedebilmeniz için internetinizi açmanız gerekmektedir.");


        networkAlert = alertBuilder.create();
        networkAlert.setCancelable(false);
        networkAlert.show();

    }

    private Drawable setImageFromAsset(String videoName) throws IOException {

        InputStream inputStream = getAssets().open(videoName);
        Drawable drawable = Drawable.createFromStream(inputStream, null);
        return drawable;
    }

}
