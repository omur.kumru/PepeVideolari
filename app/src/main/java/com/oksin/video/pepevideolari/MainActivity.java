package com.oksin.video.pepevideolari;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

//UA-85535072-2


public class MainActivity extends AppCompatActivity {

    public static String SCREEN_NAME = "ListScreen";
    InterstitialAd mInterstitialAd;

    boolean timeFlag = true;
    AlertDialog networkAlert;
    IntentFilter intentFilter;
    AlertDialog.Builder alertBuilder;
    BroadcastReceiver networkChangeReceiver;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_main);

        initViews();


    }

    @Override
    protected void onResume() {
        super.onResume();

        initAd();

        registerReceiver(networkChangeReceiver, intentFilter);

        final Handler handler = new Handler();

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                displayInterstitial();
                timeFlag = false;
                Log.d("showAd", "showAd");

            }
        };
        handler.postDelayed(runnable, 8000);

        if (timeFlag == false) {

            handler.removeCallbacks(runnable);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (networkChangeReceiver != null) {

            unregisterReceiver(networkChangeReceiver);
        }
        MainActivity.this.finish();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MainActivity.this.finish();

        System.exit(1);//added

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    private void initViews() {

        Utils.getTracker(MainActivity.this, SCREEN_NAME);
        askPermission();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        TextView mTitle = (TextView) toolbar.findViewById(R.id.toolbar_title);

        getSupportActionBar().setDisplayShowTitleEnabled(false);


        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        Fabric.with(this, new Crashlytics());

        broadCastReceiverNetwork();

    }


    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new FirstTab(), "Eski Bölümler");
        adapter.addFragment(new SecondTab(), "Yeni Bölümler");
        adapter.addFragment(new DownloadedVideoTab(), "İndirilen Bölümler");
        viewPager.setAdapter(adapter);
    }

    private void initAd() {

        mInterstitialAd = new InterstitialAd(MainActivity.this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3329588112236907/5222479676");

        AdRequest adRequestIn = new AdRequest.Builder()
                // .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                // .addTestDevice("E8CEE1A5925198EC7B7DD03CC4BAD6B9")
                .setRequestAgent("android_studio:ad_template").build();

        mInterstitialAd.loadAd(adRequestIn);

    }

    private void displayInterstitial() {

        mInterstitialAd.show();

       /* mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

                mInterstitialAd.show();
            }
        });*/

    }

    private void broadCastReceiverNetwork() {

        intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);

        networkChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if (!isNetworkConnected()) {
                    showInternetDialog();
                    Log.d("networkConnectionMain", "networkConnectionMainOff");
                } else {

                    if (networkAlert != null) {

                        networkAlert.cancel();
                        networkAlert.dismiss();
                        alertBuilder.create().dismiss();
                        Log.d("networkConnectionMain", "networkConnectionMainOn");
                    }

                }
            }
        };
    }

    public void showInternetDialog() {

        LayoutInflater inflater = getLayoutInflater();
        View viewTitle = inflater.inflate(R.layout.titlebar, null);

        alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.AlertDialogCustom));
        alertBuilder.setCancelable(true);
        alertBuilder.setCustomTitle(viewTitle);
        alertBuilder.setMessage("İnternet mevcut değil. Bölümleri telefonunuza kaydedebilmeniz için internetinizi açmanız gerekmektedir.");

        networkAlert = alertBuilder.create();
        networkAlert.setCancelable(false);
        networkAlert.show();

    }

    private void askPermission() {

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                LayoutInflater inflater = getLayoutInflater();
                View view = inflater.inflate(R.layout.titlebar, null);

                TextView titleText = (TextView) view.findViewById(R.id.titleText);
                titleText.setText("İzin Gerekli");
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(new ContextThemeWrapper(MainActivity.this, R.style.AlertDialogCustom));
                alertBuilder.setCancelable(true);
                alertBuilder.setCustomTitle(view);
                alertBuilder.setTitle("İzin Gerekli");
                alertBuilder.setMessage("Uygulamayı kullanabilmeniz ve bölümleri telefonunuza kaydedebilmeniz için depolama izni vermeniz gerekmektedir.");
                alertBuilder.setPositiveButton(R.string.anladım, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                        }


                    }
                });

                AlertDialog alert = alertBuilder.create();
                alert.show();

            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }

            }

        }
    }


    private boolean isNetworkConnected() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
