package com.oksin.video.pepevideolari;

import android.graphics.drawable.Drawable;



public class Video {


    public String videoChapterName;
    public String videoDurationChapter;
    public Drawable imageDrawable;

    public Video (String videoChapterName, String videoDurationChapter, Drawable imageDrawable) {

        this.videoChapterName = videoChapterName;
        this.videoDurationChapter = videoDurationChapter;
        this.imageDrawable = imageDrawable;

    }
}
