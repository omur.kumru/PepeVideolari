package com.oksin.video.pepevideolari;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;



public class DownloadedVideoTab extends Fragment {

    private ListView listView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initViews();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.downloaded_videos, container, false);

        if (!StringUtils.videoName.isEmpty() && !StringUtils.videoDuration.isEmpty() && !StringUtils.videoDrawable.isEmpty()) {

            CustomAdapter adapter = new CustomAdapter(getContext(), StringUtils.videoName, StringUtils.videoDuration, StringUtils.videoDrawable);

            listView = (ListView) rootView.findViewById(R.id.list_fragment);
            listView.setAdapter(adapter);

        } else {

            listView.setEmptyView(rootView.findViewById(R.id.list_fragment));
        }

        return  rootView;

    }

    private void initViews() {
    }
}
